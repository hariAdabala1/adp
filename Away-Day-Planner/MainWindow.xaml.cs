﻿using Away_Day_Planner.DAL;
using System;
using System.Windows;

namespace Away_Day_Planner
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public void RandomNumber_Click(object sender, RoutedEventArgs e)
        {

            Company company = new Company();
            company.data(TextCopanyName.Text);
            Console.WriteLine(TextCopanyName.Text);
        }

        public void Clear_Click(object sender, RoutedEventArgs e)
        {
            TextCopanyName.Text = "";
        }
    }
}
